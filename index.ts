import { config } from "dotenv";
import { resolve } from "path";

config({ path: resolve(__dirname, "../.env") });

import express, { Request, Response, NextFunction } from "express";
import graphqlHTTP from "express-graphql";
import cors from "cors";

import schema from "./schema";
import players from "./players.json";
import coupons from "./coupons.json";

const app = express();

app.use(cors());

app.use(
  "/graphql",
  (req: Request, res: Response, next: NextFunction) => {
    const { token } = req.query;
    if (token === process.env.DEV_TOKEN) return next();
    res.status(401).json({ text: "Unauthorized" });
  },
  graphqlHTTP({
    schema,
    graphiql: process.env.NODE_ENV !== "production",
  })
);

// Верификация ника и купона
app.use("/verify", (req: Request, res: Response) => {
  const { nick, coupon } = req.query;
  return res.status(200).json({
    nick: typeof nick !== "string" ? false : players.includes(nick),
    coupon:
      typeof coupon !== "string"
        ? false
        : coupons.some((_coupon) => _coupon.id === coupon),
  });
});

// Цена предмета с купоном
app.use("/discount", (req: Request, res: Response) => {
  const { coupon, price } = req.query;
  const discount = coupons.find((_coupon) => _coupon.id === coupon);
  if (typeof coupon !== "string" || !discount)
    return res.status(200).json({ error: "Incorrect coupon" });
  return res.status(200).json({
    price: +price - discount.amount <= 0 ? 0 : +price - discount.amount,
    remaining: discount.amount - +price <= 0 ? 0 : discount.amount - +price,
  });
});

app.get("*", (req: Request, res: Response) =>
  res.status(404).json({ error: "Invalid path" })
);

app.listen(process.env.PORT || 3000);

import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLBoolean,
} from "graphql";

import CategoryType from "./Category";
import ItemType from "./Item";
import CouponType from "./Coupon";

import categories from "../categories.json";
import coupons from "../coupons.json";
import players from "../players.json";

const QueryType = new GraphQLObjectType({
  name: "Query",
  fields: {
    category: {
      type: CategoryType,
      args: {
        id: { type: GraphQLString },
        default: { type: GraphQLBoolean },
      },
      resolve(_, { id, default: defaultCategory }) {
        if (defaultCategory)
          return categories.find((category) => category.default);
        return categories.find((category) => category.id === id);
      },
    },
    allCategories: {
      type: new GraphQLList(CategoryType),
      resolve() {
        return categories;
      },
    },
    item: {
      type: ItemType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(_, { id }) {
        return categories
          .find((category) => category.items.find((item) => item.id === id))
          .items.find((item) => item.id === id);
      },
    },
    allItems: {
      type: new GraphQLList(ItemType),
      resolve() {
        return categories
          .map((category) => category.items)
          .reduce((acc, items) => [...acc, ...items], []);
      },
    },
    coupon: {
      type: CouponType,
      args: {
        id: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(_, { id }) {
        return coupons.find((coupon) => coupon.id === id);
      },
    },
    allCoupons: {
      type: new GraphQLList(CouponType),
      resolve() {
        return coupons;
      },
    },
    allVerifiedPlayers: {
      type: new GraphQLList(GraphQLString),
      resolve() {
        return players;
      },
    },
    isPlayerVerified: {
      type: GraphQLBoolean,
      args: {
        name: { type: new GraphQLNonNull(GraphQLString) },
      },
      resolve(_, { name }) {
        return players.includes(name);
      },
    },
  },
});

export default QueryType;

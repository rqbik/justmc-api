import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
} from "graphql";

const ItemInfoType = new GraphQLObjectType({
  name: "ItemInfo",
  description: "Shop item info",
  fields: {
    header: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(new GraphQLList(GraphQLString)) },
  },
});

export default ItemInfoType;

import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLInt,
  GraphQLBoolean,
} from "graphql";

import ItemInfoType from "./ItemInfo";

const ItemType = new GraphQLObjectType({
  name: "Item",
  description: "Shop item",
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLString) },
    name: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(GraphQLString) },
    price: { type: new GraphQLNonNull(GraphQLInt) },
    discount: { type: GraphQLBoolean },
    countable: { type: GraphQLBoolean },
    info: { type: new GraphQLNonNull(new GraphQLList(ItemInfoType)) },
  }),
});

export default ItemType;

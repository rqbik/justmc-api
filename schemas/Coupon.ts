import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLScalarType,
  GraphQLInt,
} from "graphql";

const CouponType = new GraphQLObjectType({
  name: "Coupon",
  description: "Shop coupon",
  fields: () => ({
    id: { type: new GraphQLNonNull(GraphQLString) },
    expiresAt: { type: new GraphQLNonNull(GraphQLString) },
    issuedTo: { type: new GraphQLNonNull(GraphQLString) },
    amount: { type: new GraphQLNonNull(GraphQLInt) },
  }),
});

export default CouponType;

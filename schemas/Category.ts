import {
  GraphQLObjectType,
  GraphQLNonNull,
  GraphQLString,
  GraphQLList,
  GraphQLBoolean,
} from "graphql";

import ItemType from "./Item";

const CategoryType = new GraphQLObjectType({
  name: "Category",
  description: "Shop category",
  fields: {
    id: { type: new GraphQLNonNull(GraphQLString) },
    default: { type: GraphQLBoolean },
    name: { type: new GraphQLNonNull(GraphQLString) },
    description: { type: new GraphQLNonNull(GraphQLString) },
    items: { type: new GraphQLNonNull(new GraphQLList(ItemType)) },
  },
});

export default CategoryType;

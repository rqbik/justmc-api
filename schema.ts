import { GraphQLSchema } from "graphql";

import QueryType from "./schemas/Query";

const schema = new GraphQLSchema({ query: QueryType });

export default schema;
